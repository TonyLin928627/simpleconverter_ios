//
//  Units.swift
//  SimpleConverter
//
//  Created by Tony Lin on 20/01/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation

func unitFromName(name:String) -> Unit?{
    
    switch name {
    case Kelvin.getName():
        return Kelvin()
    
    case Celsius.getName():
        return Celsius()
    
    case Fahrenheit.getName():
        return Fahrenheit()
        
    case Litre.getName():
        return Litre()
    case Millilitre.getName():
        return Millilitre()
        
    case Gallon.getName():
        return Gallon()
        
    default:
        return nil
    }
    
}

typealias Rule = (_: Double)->Double
protocol Unit{
    func getConversionRule(className:String) -> Rule?
    static func getName() -> String
}

let ABSOLUTE_ZERO = -273.15

class Kelvin: Unit{
 
    static func getName() -> String {
        return "Kelvin"
    }
    
    private func c1(input: Double) -> Double {
        return input
    }
    
    private func c2(input: Double) -> Double {
        return input + ABSOLUTE_ZERO
    }
    
    private func c3(input: Double) -> Double {
        return ((input + ABSOLUTE_ZERO)*1.8)+32
    }
    
    func getConversionRule(className: String) -> Rule? {
        var rtn: Rule?
        switch className {
        case "Kelvin":
            rtn = self.c1
        
        case "Celsius":
            rtn = self.c2
            
        case "Fahrenheit":
            rtn = self.c3
        
        default:
            rtn = nil
        }
        
        return rtn
    }
    
}

class Celsius: Unit{
    static func getName() -> String {
        return "Celsius"
    }

    
    private func c1(input: Double) -> Double {
        return input - ABSOLUTE_ZERO
    }
    
    private func c2(input: Double) -> Double {
        return input
    }
    
    private func c3(input: Double) -> Double {
        return (input*1.8)+32
    }
    
    func getConversionRule(className: String) -> Rule? {
        var rtn: Rule?
        switch className {
        case "Kelvin":
            rtn = self.c1
            
        case "Celsius":
            rtn = self.c2
            
        case "Fahrenheit":
            rtn = self.c3
            
        default:
            rtn = nil
        }
        
        return rtn
    }
    
}

class Fahrenheit: Unit{
    
    static func getName() -> String {
        return "Fahrenheit"
    }
    
    
    private func c1(input: Double) -> Double {
        return (input-32)/1.8 - ABSOLUTE_ZERO
    }
    
    private func c2(input: Double) -> Double {
        return (input-32)/1.8
    }
    
    private func c3(input: Double) -> Double {
        return input
    }
    
    func getConversionRule(className: String) -> Rule? {
        var rtn: Rule?
        switch className {
        case "Kelvin":
            rtn = self.c1
            
        case "Celsius":
            rtn = self.c2
            
        case "Fahrenheit":
            rtn = self.c3
            
        default:
            rtn = nil
        }
        
        return rtn
    }
    
}

class Litre: Unit{
    
    static func getName() -> String {
        return "Litre"
    }
    
    private func c1(input: Double) -> Double {
        return input
    }
    
    private func c2(input: Double) -> Double {
        return input * 1000
    }
    
    private func c3(input: Double) -> Double {
        return input * 0.264172
    }
    
    func getConversionRule(className: String) -> Rule? {
        var rtn: Rule?
        switch className {
        case "Kelvin":
            rtn = self.c1
            
        case "Celsius":
            rtn = self.c2
            
        case "Fahrenheit":
            rtn = self.c3
            
        default:
            rtn = nil
        }
        
        return rtn
    }
    
}

class Millilitre: Unit{
    
    static func getName() -> String {
        return "Millilitre"
    }
    
    private func c1(input: Double) -> Double {
        return input / 1000
    }
    
    private func c2(input: Double) -> Double {
        return input
    }
    
    private func c3(input: Double) -> Double {
        return input * 0.000264172
    }
    
    func getConversionRule(className: String) -> Rule? {
        var rtn: Rule?
        switch className {
        case "Kelvin":
            rtn = self.c1
            
        case "Celsius":
            rtn = self.c2
            
        case "Fahrenheit":
            rtn = self.c3
            
        default:
            rtn = nil
        }
        
        return rtn
    }
    
    
}

class Gallon: Unit{
    
    static func getName() -> String {
        return "Gallon"
    }
    
    
    private func c1(input: Double) -> Double {
        return input * 3.78541
    }
    
    private func c2(input: Double) -> Double {
        return input * 3785.41
    }
    
    private func c3(input: Double) -> Double {
        return input
    }
    
    func getConversionRule(className: String) -> Rule? {
        var rtn: Rule?
        switch className {
        case "Kelvin":
            rtn = self.c1
            
        case "Celsius":
            rtn = self.c2
            
        case "Fahrenheit":
            rtn = self.c3
            
        default:
            rtn = nil
        }
        
        return rtn
    }
    
}
