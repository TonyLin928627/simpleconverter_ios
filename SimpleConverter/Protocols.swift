//
//  Interfaces.swift
//  SimpleConverter
//
//  Created by Tony Lin on 20/01/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation
import RxSwift

protocol Converter{
    func generateObservable(inputValue: Value, outputUnitClassName: String) -> Observable<Value>
}

protocol Convertible{
    func convertTo(outputUnitName: String, converter: Converter) -> Observable<Value>
}

enum ConversionError: Error {
    
    case UnknownUnitNameError(String)
    
    case IncompatibleConversionError(String)
    
    case UnimplementedError(String)
}
