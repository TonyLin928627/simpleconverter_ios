//
//  RemoteConverter.swift
//  SimpleConverter
//
//  Created by Tony Lin on 20/01/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation
import RxSwift

class RemoteConverter: Converter{
    
    func generateObservable(inputValue: Value, outputUnitClassName: String) -> Observable<Value>{
        
        return Observable<Value>.create({ (observer) -> Disposable in
            
            
            observer.onError(ConversionError.UnimplementedError("Not implemented yet"))
            return Disposables.create()
            
        })
        
    }
    
}

