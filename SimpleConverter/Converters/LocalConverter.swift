//
//  LocalConverter.swift
//  SimpleConverter
//
//  Created by Tony Lin on 20/01/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation
import RxSwift

class LocalConverter: Converter{
 
    func generateObservable(inputValue: Value, outputUnitClassName: String) -> Observable<Value>{
        
        return Observable<Value>.create({ (observer) -> Disposable in
            
            
            guard let rule = inputValue.unit.getConversionRule(className: outputUnitClassName) else {
                
                observer.onError(ConversionError.IncompatibleConversionError("Cannot convert \(inputValue.unit) to \(outputUnitClassName)"))
                return Disposables.create()
                
            }
            
            let outputReading = rule(inputValue.reading)
            
            guard let outputUnit = unitFromName(name: outputUnitClassName) else {
                observer.onError(ConversionError.UnknownUnitNameError("Unknow unit name \(outputUnitClassName)"))
                return Disposables.create()
            }
            
            observer.onNext(Value(unit: outputUnit, reading: outputReading))
            observer.onCompleted()
            
            return Disposables.create()
            
        })
        
    }
    
}
