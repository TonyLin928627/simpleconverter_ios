//
//  Value.swift
//  SimpleConverter
//
//  Created by Tony Lin on 20/01/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import Foundation
import RxSwift

class Value: Convertible{
    
    let unit: Unit
    var reading: Double
    
    init(unit: Unit, reading: Double = 0.0){
        self.unit = unit
        self.reading = reading
    }
    
    func convertTo(outputUnitName: String, converter: Converter) -> Observable<Value>{
        return converter.generateObservable(inputValue: self, outputUnitClassName: outputUnitName)
    }
}

