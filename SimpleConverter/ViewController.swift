//
//  ViewController.swift
//  SimpleConverter
//
//  Created by Tony Lin on 20/01/19.
//  Copyright © 2019 Tony Lin. All rights reserved.
//

import UIKit
import iOSDropDown
import RxSwift

class ViewController: UIViewController {
    
    
    @IBOutlet weak var inputUnits: DropDown!
    @IBOutlet weak var outputUnits: DropDown!
    @IBOutlet weak var inputReading: UITextField!
    @IBOutlet weak var outputReading: UILabel!
    
    @IBOutlet weak var errMsg: UILabel!
    @IBOutlet weak var converters: UISegmentedControl!
    @IBAction func onConvertClicked(_ sender: Any) {
        print("Convert button clicked")
        
    
        errMsg.text = ""
        
        
        guard let converter = createConverter() else {
             //todo: pop unimplemented dialog
            return
        }
        
        
        outputReading.text = ""
        
        let inputUnit = self.Unites[inputUnits.selectedIndex!]
        guard let inputReadingDouble = Double(self.inputReading.text!)  else {
            return
        }
        
        
        Value(unit: inputUnit, reading:inputReadingDouble).convertTo(outputUnitName: outputUnits.text!, converter: converter)
        .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
        .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (Value) in
                print("\(Value.reading)")
                self.outputReading.text = "\(Value.reading)"
            }, onError: { (Error) in
                print("\(Error)")
                
                self.errMsg.text = "\(Error)"
                
        
            }, onCompleted: {
                print("done")
                
            }) {
                
        }
        
    }
    
    private func createConverter() -> Converter?{
        var converter: Converter?
        
        switch converters.selectedSegmentIndex {
        case 0:
            converter = LocalConverter()
            
        case 1:
            converter = RemoteConverter()
        default:
            converter = nil
        }
        
        return converter
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        initDropDowns()
        
    
        errMsg.text = ""
    }


    private let Unites: [Unit] = [Kelvin(), Celsius(), Fahrenheit(), Litre(), Millilitre(), Gallon()]
    
    private func initDropDowns(){
        let optionArray = [ "Kelvin", "Celsius", "Fahrenheit", "Litre", "Millilitre", "Gallon"]
        let optionIds = [0, 1, 2, 3, 4, 5]
        
        inputUnits.optionArray = optionArray
        inputUnits.optionIds = optionIds
        
        inputUnits.text = inputUnits.optionArray[0]
        inputUnits.selectedIndex = 0
        
        inputUnits.didSelect{(selectedText , index ,id) in
            print("input unit is \(selectedText),\(id), \(index) ")
        }

        outputUnits.optionArray = optionArray
        outputUnits.optionIds = optionIds
        
        outputUnits.text = outputUnits.optionArray[0]
        outputUnits.selectedIndex = 0
        
        outputUnits.didSelect{(selectedText , index ,id) in
            print("input unit is \(selectedText),\(id), \(index) ")
        }
    }
}

